<?php session_start();
include('connect.php');
$_SESSION['soi'] = '1111';
unset($_SESSION['trivia']);
unset($_SESSION['totpreg_user']);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Tu aliado incondicional en liquidación de Seguridad Social, has clic aquí para resolver todas tus dudas sobre la nueva normativa.">
    <meta name="author" content="SOI Compromiso">
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
		<!-- Open Graph data -->
		<meta property="og:title" content="SOI Compromiso" />
		<meta property="og:type" content="article" />
		<meta property="og:url" content="http://www.soicompromiso.com" />
		<meta property="og:image" content="http://www.soicompromiso.com/ach.jpg" />
		<meta property="og:description" content="Tu aliado incondicional en liquidación de Seguridad Social, has clic aquí para resolver todas tus dudas sobre la nueva normativa." />
		<meta property="og:site_name" content="SOI Compromiso" />
    <title>SOI Compromiso</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/icomoon.css">
    <link href="assets/css/animate-custom.css" rel="stylesheet">
		<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="assets/css/sweetalert.css" />
    <link rel="stylesheet" href="assets/css/ladda.min.css">
    <link rel="stylesheet" href="css/icons/style.css" />
    <link rel="stylesheet" href="css/shadowbox.css" />
    <link rel="stylesheet" href="css/style.css" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js"></script>
      <script src="assets/js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body data-spy="scroll" data-offset="0" data-target="#navbar-main">
  	<?php include('gtm.php'); ?>
  	<aside class="log-tendencia">
  		<figure>
				<img src="images/logo-vigilado.png" alt="vigilado" />
			</figure>
  	</aside>
  	<div id="navbar-main">
      <!-- Fixed navbar -->
	    <div class="navbar navbar-inverse navbar-fixed-top">
	      <div class="container">
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	            <span class="icon-menu"></span>
	          </button>
	          <a class="navbar-brand hidden-xs hidden-sm" href="index.php">
	          	<figure id="logo">
	            	<img src="images/logo.png" alt="" />
	            </figure>
	          </a>
	        </div>
	        <div class="navbar-collapse collapse">
	          <ul class="nav navbar-nav pull-right visible-sm visible-md visible-lg">
	            <li><a href="#home" class="smoothScroll">Inicio</a></li>
							<li> <a href="#tittriv" class="smoothScroll"> Trivia</a></li>
							<li> <a href="#services" class="smoothScroll"> Capacitaciones y Eventos</a></li>
	          </ul>
	          <ul class="nav navbar-nav visible-xs">
	            <li><a href="#home" class="smoothScroll">Inicio</a></li>
							<li> <a href="#tittriv" class="smoothScroll"> Trivia</a></li>
							<li> <a href="#services" class="smoothScroll"> Capacitaciones y Eventos</a></li>
	          </ul>
					</div><!--/.nav-collapse -->
	      </div>
	    </div>
    </div>
		<!-- ==== HEADERWRAP ==== -->
		<div id="headerwrap" id="home" name="home">
			<header class="clearfix">
				<div class="col-md-8 col-md-offset-2">
					<div class="vid">
                    <?php 
					//obtener información del video
					$v1 = "select * from videos order by rand() limit 0,1";
					if(!$v2 = mysql_query($v1))echo mysql_error();
					$v3 = mysql_fetch_array($v2);
					?>
						<iframe src="https://www.youtube.com/embed/<?php echo $v3['video']."?enablejsapi=1&rel=0";?>" frameborder="0" allowfullscreen></iframe>
 					</div><!--./vid -->
 				</div>
			</header>	 
			<asdie class="texto-video">
				Resolución 2388 de 2016
			</asdie>   
		</div><!-- /headerwrap -->
		<section class="trivia">
			<a href="#greywrap" class="smoothScroll">Ir a la trivia</a>
			<div id="tittriv"></div>
		</section>
		<!-- ==== GREYWRAP ==== -->
		<div id="greywrap">
			<div class="row">
				<div class="col-lg-12 callout">
					<figure class="logo2">
						<img src="images/logo-c.png" alt="logo" />
					</figure>
					<hgroup class="subt1">
						<h2>¿COMENZAMOS?</h2>
						<h3 class="text-center">
							Para empezar con la trivia necesitamos conocer tus datos, será algo muy rápido.
						</h3>
					</hgroup>
					<div class="col-lg-7 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
						<form class="form-horizontal" role="form" id="regf" name="regf">
							<div class="form-group">
								<label for="nombre" class="col-lg-4 control-label"></label>
								<div class="col-lg-10">
									<input type="text" class="form-control" id="nombre" name="nombre" placeholder="Ingresa tu nombre">
								</div>
							</div>
							<div class="form-group">
								<label for="apellido" class="col-lg-4 control-label"></label>
								<div class="col-lg-10">
									<input type="text" class="form-control" id="apellido" name="apellido" placeholder="Apellido">
								</div>
							</div>
							<div class="form-group">
								<label for="email" class="col-lg-4 control-label"></label>
								<div class="col-lg-10">
									<input type="email" class="form-control" id="email" name="email" placeholder="Email">
								</div>
							</div>
							<div class="form-group">
								<label for="empresa" class="col-lg-4 control-label"></label>
								<div class="col-lg-10">
									<input type="text" class="form-control" id="empresa" name="empresa" placeholder="Empresa">
								</div>
						  </div>
							<div class="form-group">
								<label for="cargo" class="col-lg-4 control-label"></label>
								<div class="col-lg-10">
									<input type="text" class="form-control" id="cargo" name="cargo" placeholder="Cargo">
								</div>
						  </div>
                          <div class="form-group">
								<label for="nit" class="col-lg-4 control-label"></label>
								<div class="col-lg-10">
									<input type="text" class="form-control" id="nit" name="nit" placeholder="NIT">
								</div>
						  </div>
							<div class="form-group">
								<label for="telefono" class="col-lg-4 control-label"></label>
								<div class="col-lg-10">
									<input type="text" class="form-control" id="telefono" name="telefono" placeholder="Teléfono (Opcional)">
								</div>
						  </div>
                          <div class="form-group">
      <div class="checkbox">
  		<label><input type="checkbox" value="s" id="acepto1" name="acepto1" checked class="pull-left">Acepto los términos y condiciones</label>
		</div>
      </div>
      
      <div class="form-group">
      <div class="checkbox">
  		<label><input type="checkbox" value="s"  id="acepto2" name="acepto2">Acepto recibir información</label>
		</div>
      </div>
							<div class="form-group">
								<div class="col-lg-10">
									<button class="ladda-button" data-color="mint" data-style="expand-right" data-size="xs" id="participar" name="participar" onClick="registro();" type="button">
										<span class="ladda-label">Participar</span>
										<span class="ladda-spinner"></span>
										<div class="ladda-progress" style="width: 89px;"></div>
									</button>
						    </div>
						  </div>
						</form><!-- form -->
					</div>
				</div>
			</div><!-- row -->
		</div><!-- greywrap -->
		<!-- ==== SERVICES ==== -->
		<section class="services" id="services" name="services">
			<div class="row">
				<h2 class="centered">Capacitaciones y Eventos</h2>
				<div class="col-lg-offset-2 col-lg-8 textoM">
					<p>Foros regionales Cali - 4 de agosto 2016</p>
					<div class="flex galeriregion">
						<?php
							for ($ig=1; $ig < 10; $ig++) {
						?>
						<figure>
							<a href="images/regionales/IMG_<?php echo $ig; ?>.jpg" rel="shadowbox[Vacation]">
								<img src="images/regionales/IMG_<?php echo $ig; ?>.jpg" alt="imagen" />
							</a>
						</figure>
						<?php
							}
						?>
					</div>
					<p>Los cambios que trajo la Resolución 2388 de 2016. Bogotá 10 de agosto</p>
					<p>
						Hotel Cosmos 100, 2:00 p.m. a 5:00 p.m.<br />
					</p>
					<div class="flex galeriregionB">
						<?php
							for ($ag=1; $ag < 7; $ag++) {
						?>
						<figure>
							<a href="images/regionales/AMG_<?php echo $ag; ?>.jpg" rel="shadowbox[Vacation]">
								<img src="images/regionales/AMG_<?php echo $ag; ?>.jpg" alt="imagen" />
							</a>
						</figure>
						<?php
							}
						?>
					</div>
					<p>Foros Regionales:</p>
					<p>Barranquilla 8 Septiembre<br>Bucaramanga 6 Octubre<br>Pereira 2 Noviembre</p>
					<p>Capacitaciones:</p>
					<p>SOI te entrena en la resolución 2388.<br>Asiste a nuestra capacitación donde vamos a explicar punto a punto los cambios.<br>Bogotá 10 de Agosto. (Hotel Cosmos Av 100 # 19A-83)<br>Medellin 19 de Agosto</p>
				</div><!-- col-lg -->
			</div><!-- row -->
		</section><!-- container -->	
		<footer id="footerwrap">
			<div class="container">
				<h4>ACH Colombia</h4>
			</div>
			<div class="creditos">
				<div class="margen">
					<article class="end-footer flex flex-between flex-align-center">
						<article class="vigilado">
							<figure>
								<img src="images/logo-vigilado.png" alt="vigilado" />
							</figure>
						</article>
						<article class="direcion-redes flex flex-column flex-between">
							<div class="flex colunm cl1">
								<div class="red" data-url="https://twitter.com/ACH_Informa">
									<span class="col-icon icon-twitter-ach"></span>
									<span> @ ACH_Informa</span>
								</div>
								<div class="red" data-url="https://www.facebook.com/TransaccionesACH/?fref=ts">
									<span class="col-icon icon-facebook-ach"></span>
									<span> Transacciones ACH</span>
								</div>
								<div class="red" data-url="https://www.linkedin.com/company/transacciones-ach?trk=nav_account_sub_nav_company_admin">
									<span class="col-icon icon-in-ach"></span>
									<span> Transacciones ACH</span>
								</div>
							</div>
							<div class="flex colunm cl2">
								<div class="cc">2016 ACH Colombia</div>
								<div class="cc">Bogotá: 380 8080</div>
								<div class="cc">Resto del País: 01 8000 11 0764</div>
								<div class="cc">Tv: 23 #97 73, Bogotá, piso 3</div>			
							</div>
						</article>
					</article>
				</div>
			</div>
		</footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	<script src="assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/modernizr.custom.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/retina.js"></script>
	<script type="text/javascript" src="assets/js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="assets/js/smoothscroll.js"></script>
	<script type="text/javascript" src="assets/js/jquery-func.js"></script>
	<script type="text/javascript" src="assets/js/sweetalert-min.js"></script>
	<script type="text/javascript" src="assets/js/spin.min.js"></script>
	<script type="text/javascript" src="assets/js/ladda.min.js"></script>
	<script type="text/javascript" src="assets/js/soi.js"></script>
	<script type="text/javascript" src="assets/js/shadowbox.js"></script>
	<script type="text/javascript" src="assets/js/script.js"></script>
  </body>
</html>
