<?php session_start();

include('connect.php');

if($_SESSION['trivia']==''){
	header('Location:index.php');
	end();
}
 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="SHIELD - Free Bootstrap 3 Theme">
    <meta name="author" content="Grancomunicaciones">
    <link rel="shortcut icon" href="assets/ico/favicon.ico">

    <title>Landing Page SOI</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/icomoon.css">
    <link href="assets/css/animate-custom.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="assets/css/sweetalert.css" />
    <link rel="stylesheet" href="assets/css/ladda.min.css">
    <link rel="stylesheet" href="css/icons/style.css" />
    <link rel="stylesheet" href="css/style.css" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js"></script>
      <script src="assets/js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body data-spy="scroll" data-offset="0" data-target="#navbar-main">
  <?php include('gtm.php'); ?>
    <aside class="log-tendencia">
      <figure>
        <img src="images/logo-vigilado.png" alt="vigilado" />
      </figure>
    </aside>
  	<div id="navbar-main">
      <!-- Fixed navbar -->
      <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="icon-menu"></span>
            </button>
            <a class="navbar-brand hidden-xs hidden-sm" href="index.php">
              <figure id="logo">
                <img src="images/logo.png" alt="" />
              </figure>
            </a>
          </div>
          <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav pull-right visible-sm visible-md visible-lg">
              <li><a href="index.php" class="smoothScroll">Inicio</a></li>
            </ul>
            <ul class="nav navbar-nav visible-xs">
              <li><a href="index.php" class="smoothScroll">Inicio</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>
    <section class="secG sec-gris">     
    	<!-- ==== GREYWRAP ==== -->
  		<div id="greywrap">
        <?php 
			//traer pregunta en la que va el usuario
			$idpreg = traer_pregunta_id($_SESSION['totpreg_user']);
			$texto = traer_pregunta($idpreg);
			$opc1 = traer_opcion($idpreg,1);
			$opc2 = traer_opcion($idpreg,2);
			$opc3 = traer_opcion($idpreg,3);
			$opc4 = traer_opcion($idpreg,4);
			?>
  			<div class="row">
  				<div class="col-lg-12 callout">
  					<figure class="logo2">
              <img src="images/logo-c.png" alt="logo" />
            </figure>
            <hgroup class="subt1">
              <h2>Trivia soi compromiso</h2>
              <h3>
                <?php echo $texto; ?>
              </h3>
            </hgroup>
            <article class="preguntas">
              <form action="#" method="post" class="formdise input-checks-radio1" id="respu" name="respu">
                <div class="resp">
                  <input type="radio" id="res1" name="respuestas" value="1" />
                  <label for="res1">
                    <span class="span-form"></span> 
                    <?php echo $opc1; ?>
                  </label>
                </div>
                <div class="resp">
                  <input type="radio" id="res2" name="respuestas" value="2" />
                  <label for="res2">
                    <span class="span-form"></span> 
                    <?php echo $opc2; ?> 
                  </label>
                </div>
                <div class="resp">
                  <input type="radio" id="res3" name="respuestas" value="3" />
                  <label for="res3">
                    <span class="span-form"></span> 
                    <?php echo $opc3; ?>
                  </label>
                </div>
                <div class="resp">
                  <input type="radio" id="res4" name="respuestas" value="4" />
                  <label for="res4">
                    <span class="span-form"></span> 
                    <?php echo $opc4; ?>
                  </label>
                </div>
                
                <div class="botondiv">
                  <button class="botonP button-green" type="button" onClick="responder();">Siguiente</button>
                </div>
                  <div class="botondiv">
                  Pregunta <?php echo $_SESSION['totpreg_user']+1; ?> de <?php echo $_SESSION['totpreg']; ?> 
                </div>
              </form>
            </article>
          </div>
        </div><!-- row -->
  		</div><!-- greywrap -->
    </section>
  	<footer id="footerwrap">
      <div class="container">
        <h4>ACH Colombia</h4>
      </div>
      <div class="creditos">
        <div class="margen">
          <article class="end-footer flex flex-between flex-align-center">
            <article class="vigilado">
              <figure>
                <img src="images/logo-vigilado.png" alt="vigilado" />
              </figure>
            </article>
            <article class="direcion-redes flex flex-column flex-between">
              <div class="flex colunm cl1">
                <div class="red" data-url="https://twitter.com/ACH_Informa">
                  <span class="col-icon icon-twitter-ach"></span>
                  <span> @ ACH_Informa</span>
                </div>
                <div class="red" data-url="https://www.facebook.com/TransaccionesACH/?fref=ts">
                  <span class="col-icon icon-facebook-ach"></span>
                  <span> Transacciones ACH</span>
                </div>
                <div class="red" data-url="https://www.linkedin.com/company/transacciones-ach?trk=nav_account_sub_nav_company_admin">
                  <span class="col-icon icon-in-ach"></span>
                  <span> Transacciones ACH</span>
                </div>
              </div>
              <div class="flex colunm cl2">
                <div class="cc">2016 ACH Colombia</div>
                <div class="cc">Bogotá: 380 8080</div>
                <div class="cc">Resto del País: 01 8000 11 0764</div>
                <div class="cc">Tv: 23 #97 73, Bogotá, piso 3</div>     
              </div>
            </article>
          </article>
        </div>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.min.js"></script>
  	<script type="text/javascript" src="assets/js/modernizr.custom.js"></script>
  	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
  	<script type="text/javascript" src="assets/js/retina.js"></script>
  	<script type="text/javascript" src="assets/js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="assets/js/smoothscroll.js"></script>
  	<script type="text/javascript" src="assets/js/jquery-func.js"></script>
    <script type="text/javascript" src="assets/js/sweetalert-min.js"></script>
    <script type="text/javascript" src="assets/js/spin.min.js"></script>
  	<script type="text/javascript" src="assets/js/ladda.min.js"></script>
    <script type="text/javascript" src="assets/js/soi.js"></script>
    <script type="text/javascript" src="assets/js/script.js"></script>
  </body>
</html>