$(inicio_lading);

var wv = $(window).width();

function inicio_lading () {
	$('.direcion-redes .red').on('click', direcionar);
	Shadowbox.init({
		handleOversize:"drag",
		modal:true
	});

	if (wv >= 700) {
		galeriasmostrar()
	}
	console.log(wv);
}

function direcionar () {
	var url = $(this).attr('data-url');
	window.open(url);
}

function galeriasmostrar () {
	divimages($('.galeriregion figure'));
	divimages($('.galeriregionB figure'));
}

function divimages (div) {
	var cantidad = $(div).length;
	var num = 0;
	$(div).each(mostrarcantidad(cantidad, num));
}

function mostrarcantidad (cant, num) {
	return function () {
		num++
		if (num > 4) {
			$(this).css({display:'none'});
		}
	}
}
