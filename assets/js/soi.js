function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

function registro(){
//validar campos
var error ="";
var nombre = $('#nombre').val();
var apellido = $('#apellido').val();
var email = $('#email').val();
var email2 = isEmail(email);
var empresa = $('#empresa').val();
var cargo = $('#cargo').val();
var nit = $('#nit').val();

	if(nombre==''){
		error += "- Por favor ingresa tu nombre \n";
	}
	if(apellido==''){
		error += "- Por favor ingresa tu apellido \n";
	}
	if(email==''){
		error += "- Por favor ingresa tu email \n";
	}else{
		if(!isEmail(email)){
			error += "- E-mail incorrecto \n";
		}
	}
	if(empresa==''){
		error += "- Por favor ingresa tu empresa \n";
	}
	if(cargo==''){
		error += "- Por favor ingresa tu cargo \n";
	}
	if(nit==''){
		error += "- Por favor ingresa Nit de tu empresa \n";
	}
	if($('#acepto1').is(':checked')){
	
	}else{
		error += "- Debes aceptar los terminos y condiciones \n";
	}
		if(error==''){
			var l = Ladda.create(document.querySelector('#participar'));
			l.start();
			l.setProgress(1);
			
			var hayek = $('#regf').serialize();
			$.ajax({
			type: "POST",
			url: "soicheck.php",
			cache: false,
			data :hayek+"&opc=valreg",
			success: function(data,status){
				if(data==0){
					//validar si el usuario ya realizo el proceso
					document.location = 'trivia.php'; 
				}else if(data==1){
					//validar si el usuario ya esta participante
					swal({title: "Felicitaciones", text: 'Ya haz realizado todas las preguntas de la trivia', type: "success", confirmButtonText: "OK", html: false});
				}else if(data==2){
					//dejarlo ingresar
					document.location = 'trivia.php';
				}
				var l = Ladda.create(document.querySelector('#participar'));
				l.stop();
			},
			error: function (request, status, error) {
					alert(request.responseText);
			}
			});
		}else{
			swal({title: "Completa tu información", text: error, type: "warning", confirmButtonText: "OK", html: false});
		}
	}
	
function responder(){
var respuesta = $('[name="respuestas"]:checked').length;
var respuesta_fin = $('input[name=respuestas]:checked', '#respu').val()

if(respuesta==0){
	swal({title: "Selecciona tu respuesta", type: "warning", confirmButtonText: "OK", html: false});
}else{
	//llamo ajax para validar respuesta y continuar
			var hayek = $('#respu').serialize();
			$.ajax({
			type: "POST",
			url: "soicheck.php",
			cache: false,
			data :hayek+"&opc=respuesta",
			success: function(data,status){
				if(data=='final'){
					swal({
						title: "¡FELICITACIONES!", 
						text: 'Ya tienes todo lo que se necesita para responder cualquier pregunta en el bazar<br>TU CONOCIMIENTO VALE<br>Espera más preguntas.', 
						type: "success", 
						confirmButtonText: "Siguiente", 
						closeOnConfirm: false,
						html: true
						},
						function(){   
						document.location = 'index.php'; 
						});
				}else if(data==1){
					//validar si el usuario ya realizo el proceso
					swal({
						title: "Felicitaciones", 
						text: 'Respuesta Correcta', 
						type: "success", 
						confirmButtonText: "Siguiente", 
						closeOnConfirm: false,
						html: false
						},
						function(){   
						document.location = 'trivia.php'; 
						});
				}else{
					swal({
						title: "Incorrecto", 
						text: '<b>Respuesta Correcta: </b>'+data, 
						type: "warning", 
						confirmButtonText: "Siguiente", 
						closeOnConfirm: false,
						html: true
						},
						function(){   
						document.location = 'trivia.php'; 
						});
				}
			},
			error: function (request, status, error) {
					alert(request.responseText);
			}
			});
}

}