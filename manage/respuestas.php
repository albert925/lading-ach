<?php
require_once("includes/header.inc.php");
?>
<link type="text/css" rel="stylesheet" href="css/main.css"/>
<script type="text/javascript">
var itemsPerPage = <?php echo json_encode($itemsPerPage); ?>;
var actionType = <?php echo json_encode($actionType); ?>;
var page = <?php echo json_encode($page); ?>;
var idcon = <?php echo json_encode($idcon); ?>;
var currentFile = <?php echo json_encode($currentFile); ?>;
var buscador = <?php echo json_encode($buscador); ?>;
</script>
</head>
<body>
<?php if(isset($sessionToken)) { ?>
<div class="full-width full-width-extra frame">
	<div class="sidebar sidebar-fixed">
	</div>
	<div class="sidebar">
		<div id="wrapper" class="wrapper">
		</div>
	</div>
	<div class="right-content">
		<div id="navbar" class="navbar">
		</div>
		<div id="main-container" class="col-xs-12">
			<div class="col-xs-12 text-center mainTitle">
				<?php echo ucfirst(str_replace(".php", "", $currentFile)); ?>
			</div>
			<?php
				if($buscador != ""){
			 		$queryTotal = "SELECT COUNT(usuarios_respuestas.id) AS total FROM usuarios_respuestas INNER JOIN usuarios ON idusuario=usuarios.id INNER JOIN preguntas ON idpregunta=preguntas.id WHERE UPPER(nombre) LIKE '%$buscador%' OR UPPER(apellido) LIKE '%$buscador%' OR UPPER(email) LIKE '%$buscador%' OR UPPER(pregunta) LIKE '%$buscador%'";
					$query = "SELECT * FROM usuarios INNER JOIN usuarios ON idusuario=usuarios.id INNER JOIN preguntas ON idpregunta=preguntas.id WHERE UPPER(nombre) LIKE '%$buscador%' OR UPPER(apellido) LIKE '%$buscador%' OR UPPER(email) LIKE '%$buscador%' OR UPPER(pregunta) LIKE '%$buscador%' ORDER BY usuarios_respuestas.id DESC LIMIT $inicio, $fin";
				}
				else{
					$queryTotal = "SELECT COUNT(usuarios_respuestas.id) AS total FROM usuarios_respuestas INNER JOIN usuarios ON idusuario=usuarios.id INNER JOIN preguntas ON idpregunta=preguntas.id";
					$query = "SELECT * FROM usuarios_respuestas INNER JOIN usuarios ON idusuario=usuarios.id INNER JOIN preguntas ON idpregunta=preguntas.id ORDER BY usuarios_respuestas.id DESC LIMIT $inicio, $fin";
				}
				
				if(!$resultTotal = mysqli_query($dbcon, $queryTotal)) echo mysqli_error($dbcon);
				$total = mysqli_fetch_assoc($resultTotal);
				$total = $total["total"];
			?>
			<div class="col-xs-12">
				<form id="finder" name="finder" method="post" action="" class="form-inline" role="form">
					<div id="searchGroup" class="form-group separator-right separator-bottom col-xs-12">
						<div class="input-group" style="float:left">
							<span class="input-group-btn">
								<button class="btn btn-default" type="button" style="margin-top:0"
									onclick="return buscar();" onKeyPress="return buscar();" ><i class="fa fa-search"></i></button>
							</span>
							<input id="buscador" name="buscador" type="text" class="form-control" placeholder="Buscar...">
							<span class="input-group-btn">
								<button type="button" class="btn btn-default" style="margin-top:0"
									onclick="MM_goToURL('self','<?php echo $currentFile; ?>');return document.MM_returnValue">Ver todos</button>
							</span>
						</div>
						<div class="col-xs-1">
						<?php $sqlReporte = "SELECT pregunta, respuesta, nombre, apellido, email FROM usuarios_respuestas INNER JOIN usuarios ON idusuario=usuarios.id INNER JOIN preguntas ON idpregunta=preguntas.id"; ?>
						<a href="services/ReportService.php?sql=<?php echo $sqlReporte."&name=respuestas"; ?>" target="_blank" data-toggle="tooltip" title="Reporte">
						<span class="btn btn-success" style="margin-top:0"><i class="fa fa-file-text-o main-icon"></i></span></a>
						</div>
					</div>
				</form>
				<div class="table-responsive2 separator-top">
					<table id="mainTable" class="table table-hover">
						<thead>
							<tr>
                            	<th>Pregunta</th>
                                <th>Respuesta</th>
								<th>Nombre</th>
								<th>Apellido</th>
								<th>Email</th>
                                <th>Fecha</th>
							</tr>
						</thead>
						<tbody>
							<?php
							if(!$result = mysqli_query($dbcon, $query)) echo mysqli_error($dbcon);
							while($row = mysqli_fetch_array($result)){
							?>
							<tr>
								<td><?php echo $row['pregunta']; ?></td>
								<td><?php echo $row['opcion'.$row['respuesta']]; ?></td>
								<td><?php echo $row['nombre']; ?></td>
								<td><?php echo $row['apellido']; ?></td>
                                <td><?php echo $row['email']; ?></td>
                                <td><?php echo $row['fecha']; ?></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
				<div id="paginator" class="col-xs-12">
				</div>
			</div>
			<?php echo "<script> var total = ".$total."; </script>"; ?>
		</div>
		<!--main-container-->
	</div>
	<!--right-content-->
</div>
<?php }else{ ?>
<div id="lostConnection">
<?php require_once("utils/components/lostConnection.php"); ?>
</div>
<?php } require_once("includes/footer.inc.php"); ?>
<script type="text/javascript" src="js/respuestas.js"></script>
</body>
</html>