<?php
session_start();
//header("Cache-control: private"); // Arregla IE 6
require_once("config.php");

if(isset($_SESSION[$sessionUserID]))
	unset($_SESSION[$sessionUserID]);
if(isset($_SESSION[$sessionPassID]))
	unset($_SESSION[$sessionPassID]);
if(isset($_SESSION[$sessionTokenID]))
	unset($_SESSION[$sessionTokenID]);
	
//session_destroy();

header("Location: index.php");
echo "<html></html>";
exit; 
?> 