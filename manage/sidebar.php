<?php 
session_start();
require_once("config.php");
?>
<a href="javascript:void(0);" class="logo"><img src="images/logo.png" alt="" class="img-responsive" style="margin:0 auto;"/></a>
<ul class="nav nav-list separator-top">
	<li><a class="dropdown" href="#"><i class="fa fa-reorder"></i>Contenidos<span class="label">0</span></a>
		<ul>
			<li><a href="videos.php"><i class="fa fa-youtube"></i>Videos</a></li>
			<li><a href="preguntas.php"><i class="fa fa-question-circle"></i>Preguntas</a></li>
		</ul>
	</li>
    
    <li><a class="dropdown" href="#"><i class="fa fa-reorder"></i>Reportes<span class="label">0</span></a>
		<ul>
        	<li><a href="usuarios.php"><i class="fa fa-user"></i>Usuarios</a></li>
			<li><a href="respuestas.php"><i class="fa fa-check-circle"></i>Respuestas</a></li>
		</ul>
	</li>
</ul>

<script type="text/javascript">
$(document).ready(function() {
	$('.sidebar .nav > li  a.dropdown').click(function(e){
		e.preventDefault();
		$(this).next('ul').slideToggle();
	});
	
	$('.sidebar .nav > li  a.dropdown').parent("li").each(function( index ) {
		$(this).children("a.dropdown").children("span").text($(this).children("ul").children().length);
	});
});
</script>
