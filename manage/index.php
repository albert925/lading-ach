<?php
require_once("includes/header.inc.php");
?>
<link type="text/css" rel="stylesheet" href="css/main.css"/>
<script type="text/javascript">
var currentFile = <?php echo json_encode($currentFile); ?>;
</script>
</head>
<body onload="document.pageForm.user.focus();">
<div class="container">
	<div class="row">
		<div class="col-xs-10 col-xs-push-1 col-xs-pull-1 col-md-4 col-md-push-4 col-md-pull-4 text-center">
			<img src="images/logo.png" alt="" border="0" class="logoIndex img-responsive"/>
			<form id="pageForm" name="pageForm" role="form" class="form-horizontal" enctype="multipart/form-data" action="services/LoginService.php" method="post">
				<div class="form-group">
					<label for="user" class="control-label sr-only">Usuario</label>
					<div class="col-xs-12">
						<input id="user" name="user" type="text" class="form-control" placeholder="Usuario">
					</div>
				</div>
				<div class="form-group">
					<label for="password" class="control-label sr-only">Password</label>
					<div class="col-xs-12">
						<input id="password" name="password" type="password" class="form-control" placeholder="Password">
					</div>
				</div>
				<input type="submit" value="Ingresar" class="btn btn-lg btn-success">
			</form>
		</div>
	</div>
</div>
<?php require_once("includes/footer.inc.php"); ?>
<script type="text/javascript" src="js/index.js"></script>
</body>
</html>
