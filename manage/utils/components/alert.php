<div id="dialogoverlay" class="modal fade" role="dialog">
<div id="dialogbox" class="modal-dialog">
<div class="modal-content">
<div id="alerthead" class="modal-header"></div>
<div id="alertbody" class="modal-body"></div>
<div id="alertfooter" class="modal-footer">
<button id="closeAlertBtn" type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
</div>
</div>
</div>
</div>