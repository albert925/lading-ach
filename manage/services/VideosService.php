<?php
session_start();
require_once("../config.php");
require_once("../plugins/youtube.class.php");

$response = new stdClass();

if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
	if(isset($sessionToken)) {
		$actionType = getPostParam($dbcon, 'actionType');
		$idcon = getPostParam($dbcon, 'idcon');
		$page = getPostParam($dbcon, 'page');
		
		$titulo = getPostParam($dbcon, 'titulo');
		$video = getPostParam($dbcon, 'video');
		
		$numVid = getPostParam($dbcon, 'numVid');
		
		if($actionType == "addVideo"){
			$youtube = new YouTube();
			$videoID = $youtube->GetVideoIdFromUrl($video);
			if(!$videoID)
			{
				$response->code = YOUTUBE_ERROR_CODE;
				$response->error = YOUTUBE_ERROR;
				closeService($response, $dbcon);
			}
			$imagen = $youtube->GetImg($videoID, 0);
			$video = $videoID;
			
			$query = "INSERT INTO videos (titulo, imagen, video) VALUES ('$titulo', '$imagen', '".$video."')";
			if(!$result = mysqli_query($dbcon, $query))
			{
				$response->code = SQL_ERROR_CODE;
				$response->error = SQL_ERROR;
				if($debugServices) $response->error .= "<br/><br/>".mysqli_error($dbcon)."<br/><br/>".$query;
				closeService($response, $dbcon);
			}
			else
			{
				$response->code = NO_ERROR_CODE;
				$response->result = "";
				$response->parentUrl = "?page=".$page;
			}
		}else if($actionType == "editVideo"){
			if($video != "")
			{
				$youtube = new YouTube();
				$videoID = $youtube->GetVideoIdFromUrl($video);
				if(!$videoID)
				{
					$response->code = YOUTUBE_ERROR_CODE;
					$response->error = YOUTUBE_ERROR;
					closeService($response, $dbcon);
				}
				$imagen = $youtube->GetImg($videoID, 0);
				$video = $videoID;
				
				$query = "UPDATE videos SET imagen='$imagen', video='$video' WHERE id='$idcon'";
				if(!$result = mysqli_query($dbcon, $query))
				{
					$response->code = SQL_ERROR_CODE;
					$response->error = SQL_ERROR;
					if($debugServices) $response->error .= "<br/><br/>".mysqli_error($dbcon)."<br/><br/>".$query;
					closeService($response, $dbcon);
				}
			}
			
			$query = "UPDATE videos SET titulo='$titulo' WHERE id='$idcon'";
			if(!$result = mysqli_query($dbcon, $query))
			{
				$response->code = SQL_ERROR_CODE;
				$response->error = SQL_ERROR;
				if($debugServices) $response->error .= "<br/><br/>".mysqli_error($dbcon)."<br/><br/>".$query;
				closeService($response, $dbcon);
			}
			else
			{
				$response->code = NO_ERROR_CODE;
				$response->result = "";
				$response->parentUrl = "?page=".$page;
			}
		}else if($actionType == "deleteVideo"){
			$query = "DELETE FROM videos WHERE id='$idcon'";
			if(!$result = mysqli_query($dbcon, $query))
			{
				$response->code = SQL_ERROR_CODE;
				$response->error = SQL_ERROR;
				if($debugServices) $response->error .= "<br/><br/>".mysqli_error($dbcon)."<br/><br/>".$query;
				closeService($response, $dbcon);
			}
			else
			{
				$response->code = NO_ERROR_CODE;
				$response->result = "";
				$response->parentUrl = "?page=".$page;
			}
		}else if($actionType == "addVideos"){		
			for($i = 1; $i <= $numVid; $i++) {
				$titulo = getPostParam($dbcon, 'titulo'.$i);
				$video = getPostParam($dbcon, 'video'.$i);
				
				$youtube = new YouTube();
				$videoID = $youtube->GetVideoIdFromUrl($video);
				if(!$videoID)
				{
					$response->code = YOUTUBE_ERROR_CODE;
					$response->error = YOUTUBE_ERROR;
					closeService($response, $dbcon);
				}
				$imagen = $youtube->GetImg($videoID, 0);
				$video = $videoID;
			
				$query = "INSERT INTO videos (titulo, imagen, video) VALUES ('$titulo', '$imagen', '".$video."')";
				if(!$result = mysqli_query($dbcon, $query))
				{
					$response->code = SQL_ERROR_CODE;
					$response->error = SQL_ERROR;
					if($debugServices) $response->error .= "<br/><br/>".mysqli_error($dbcon)."<br/><br/>".$query;
					closeService($response, $dbcon);
				}
				else
				{
					$response->code = NO_ERROR_CODE;
					$response->result = "";
					$response->parentUrl = "?page=".$page;
				}
			}
		}
	} else {
		$response->code = SESSION_ERROR_CODE;
		$response->error = SESSION_ERROR;
	}
} else {
	$response->code = AJAX_ERROR_CODE;
	$response->error = AJAX_ERROR;
}
closeService($response, $dbcon);
?>