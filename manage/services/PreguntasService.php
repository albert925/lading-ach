<?php
session_start();
require_once("../config.php");

$response = new stdClass();

if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
	if(isset($sessionToken)) {
		$actionType = getPostParam($dbcon, 'actionType');
		$idcon = getPostParam($dbcon, 'idcon');
		$page = getPostParam($dbcon, 'page');
		
		$pregunta = getPostParam($dbcon, 'pregunta');
		$opcion1 = getPostParam($dbcon, 'opcion1');
		$opcion2 = getPostParam($dbcon, 'opcion2');
		$opcion3 = getPostParam($dbcon, 'opcion3');
		$opcion4 = getPostParam($dbcon, 'opcion4');
		$correcta = getPostParam($dbcon, 'correcta');
	
		if($actionType == "add"){
			$query = "INSERT INTO preguntas (pregunta, opcion1, opcion2, opcion3, opcion4, correcta) 
				VALUES ('$pregunta', '$opcion1', '$opcion2', '$opcion3', '$opcion4', '$correcta')";
			if(!$result = mysqli_query($dbcon, $query))
			{
				$response->code = SQL_ERROR_CODE;
				$response->error = SQL_ERROR;
				if($debugServices) $response->error .= "<br/><br/>".mysqli_error($dbcon)."<br/><br/>".$query;
				closeService($response, $dbcon);
			}
			else
			{
				$response->code = NO_ERROR_CODE;
				$response->result = "";
				$response->parentUrl = "?page=".$page;
			}
		}else if($actionType == "edit"){
			$query = "UPDATE preguntas SET pregunta='$pregunta', opcion1='$opcion1', opcion2='$opcion2', opcion3='$opcion3', opcion3='$opcion3', correcta='$correcta' WHERE id='$idcon'";
			if(!$result = mysqli_query($dbcon, $query))
			{
				$response->code = SQL_ERROR_CODE;
				$response->error = SQL_ERROR;
				if($debugServices) $response->error .= "<br/><br/>".mysqli_error($dbcon)."<br/><br/>".$query;
				closeService($response, $dbcon);
			}
			else
			{
				$response->code = NO_ERROR_CODE;
				$response->result = "";
				$response->parentUrl = "?page=".$page;
			}
		}else if($actionType == "delete"){
			$query = "DELETE FROM preguntas WHERE id='$idcon'";
			if(!$result = mysqli_query($dbcon, $query))
			{
				$response->code = SQL_ERROR_CODE;
				$response->error = SQL_ERROR;
				if($debugServices) $response->error .= "<br/><br/>".mysqli_error($dbcon)."<br/><br/>".$query;
				closeService($response, $dbcon);
			}
			else
			{
				$response->code = NO_ERROR_CODE;
				$response->result = "";
				$response->parentUrl = "?page=".$page;
			}
		}
	} else {
		$response->code = SESSION_ERROR_CODE;
		$response->error = SESSION_ERROR;
	}
} else {
	$response->code = AJAX_ERROR_CODE;
	$response->error = AJAX_ERROR;
}
closeService($response, $dbcon);
?>