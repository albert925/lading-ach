<?php
session_start();
require_once("../config.php");

$response = new stdClass();

$user = getPostParam($dbcon, 'user');
$password = getPostParam($dbcon, 'password');

$encodePass = crypt($password, "nc");

$query = "SELECT * FROM member WHERE user='$user' AND password='$encodePass'";
$result = mysqli_query($dbcon, $query);
if(!$result = mysqli_query($dbcon, $query))
{
	$response->code = SQL_ERROR_CODE;
	$response->error = SQL_ERROR;
	if($debugServices) $response->error .= "<br/><br/>".mysqli_error($dbcon)."<br/><br/>".$query;
	closeService($response, $dbcon);
}

if($result)
{
	$num = mysqli_num_rows($result);
	$usuario = mysqli_fetch_assoc($result);
	
	if($num == 0){
		$response->code = LOGIN_ERROR_CODE;
		$response->error = LOGIN_ERROR;
		closeService($response, $dbcon);
	}
	else {
		$loginToken = md5(uniqid(mt_rand(), true));
		$_SESSION[$sessionUserID] = $usuario["id"];
		$_SESSION[$sessionTokenID] = $loginToken;
		$_SESSION[$profilelUserID] = $usuario["perfil"];
		
		$response->code = NO_ERROR_CODE;
		$response->result = "";
		$response->perfil = $usuario["perfil"];
	}
}
else
{
	$response->code = DATABASE_ERROR_CODE;
	$response->error = DATABASE_ERROR;
}
closeService($response, $dbcon);
?>