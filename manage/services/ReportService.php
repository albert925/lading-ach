<?php
require_once("../config.php");

$sql = getGetParamSQL('sql');
$name = getGetParam($dbcon, 'name');

$sql2 = $_GET["sql"];

header("Content-Type: application/vnd.ms-excel");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("content-disposition: attachment;filename=".$name."-".date('Y-m-d').".xls");

$result = mysqli_query($dbcon, $sql);
if(!$result) {
    die("Fallo en el query\n".mysqli_error($dbcon)."\n".$sql);
}

$fields_num = mysqli_num_fields($result);

//echo "Total filas: ".mysqli_num_rows($result);
echo "<table border='1' cellpadding='0' cellspacing='0'><tr>";
// printing table headers
for($i=0; $i<$fields_num; $i++)
{
    $field = mysqli_fetch_field($result);
    echo "<td>{$field->name}</td>";
}
echo "</tr>\n";
// printing table rows
while($row = mysqli_fetch_row($result))
{
    echo "<tr>";
    foreach($row as $cell)
        echo "<td>".utf8_decode($cell)."</td>";

    echo "</tr>\n";
}
mysqli_free_result($result);
?>