$(document).ready(function() {
	if(actionType == "none")
		initPaginator(total);
		
	$('#pageForm').ajaxForm({ beforeSubmit: validar, 
		success: function(response) { 
			Distractor.hide();
			if(checkJSON(response))
			{
				var result = JSON.parse(response);
				if(result.code != 0)
					showError(result.code, result.error);
				else
				{
					var parsedResult = result.result;
					var parentUrl = result.parentUrl;
					if(parentUrl != undefined)
						document.location = currentFile + parentUrl;
					else if(parsedResult == "")
						document.location = currentFile;
					else 
						swal({title: "Error", type: "error", confirmButtonText: "OK", html: parsedResult});
				}
			}
		}, error: function() { 
			Distractor.hide();
			swal({title: "Error", text: "Ocurrió un error inesperado, por favor inténtelo de nuevo más tarde.", type: "error", confirmButtonText: "OK", html: false});
		}
	});
	
	$(".containerGaleria").hover(function(event) {
		var imgID = $(event.currentTarget).attr("data-imgid");
		$("#opcionesGaleria"+imgID).toggle();
	});
});

function validar(formData, jqForm, options) {
	Distractor.show();
	
	var form = jqForm[0];
	error = "";
	valido = true;
	
	if(actionType == "addVideo" || actionType == "editVideo")
	{
		if(form.titulo.value == "") 
			error += "- Por favor ingrese el título.<br/>";
		if(actionType == "addVideo") {
			if(form.video.value == "")
				error += "- Por favor ingrese el link del video.<br/>";
		}
	}
	else if(actionType == "addMultiple")
	{
		var numImg = parseInt(String($("#numVid").val()).replace("titulo", ""))
		for(var i = 1; i <= numImg; i++)
		{
			if(form["titulo"+i].value == "") 
				error += "- Por favor ingrese el título " + i + ".<br/>";
			if(form["video"+i].value == "")
				error += "- Por favor ingrese el link del video " + i + ".<br/>";
		}
	}
	
	if(error!="") {
		Distractor.hide();
		swal({title: "Alerta", type: "warning", confirmButtonText: "OK", html: error});
		valido = false;
	}
	return valido;
}

function addVideo() {
	var numVid = parseInt(String($("#numVid").val()).replace("titulo", "")) + 1;
	$("#numVid").val(numVid);
	
	var htmlForm = '<hr><div class="form-group required"><label for="titulo' + numVid + '" class="control-label col-xs-3">Título</label>';
	htmlForm += '<div class="col-xs-9"><input id="titulo' + numVid + '" name="titulo' + numVid + '" type="text" class="form-control"></div></div>';
	htmlForm += '<div class="form-group required"><label for="video' + numVid + '" class="control-label col-xs-3">Link Video (Youtube)</label>';
	htmlForm += '<div class="col-xs-9"><input id="video' + numVid + '" name="video' + numVid + '" type="text" class="form-control"></div></div>';
	
	$("#formContainer").append(htmlForm);
}