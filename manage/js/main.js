var Distractor = new CustomDistractor();
var sidebar_width = 250;

$(document).ready(function() {
	$("#wrapper").load("sidebar.php", function( response, status, xhr ) {
		var url = window.location.pathname;
		var filename = url.substring(url.lastIndexOf('/')+1);
		$(".wrapper ul li a[href='"+filename+"']" ).parent().addClass("active");
		
		$('.sidebar .nav > li  ul > li.active').parent().css('display','block');
	}); 
	
	$("#navbar").load("navbar.php", function( response, status, xhr ) {
		$('.toggle-sidebar').click(function () {
			if($('.sidebar').css("left") == "0px") {
				$('.sidebar').css("left", "-" + sidebar_width + "px");
				$('.right-content').css("margin-left", "0px");
			}
			else if($('.sidebar').css("left") == "-" + sidebar_width + "px") {
				$('.sidebar').css("left", "0px");
				$('.right-content').css("margin-left", sidebar_width + "px");
			}
		});
	});
	
	$(window).resize(function() {
		$('.sidebar').css('left', '');
		$('.right-content').css('margin-left', '');
	});
	
	$("input[type='file']").change(function(event) {
		$(this).prev(".file-label").val($(this).val().split('\\').pop());
	});
	
	$("#buscador").keydown(function(event){
		if(event.keyCode == 13) {
			event.preventDefault();
			buscar();
			return false;
		}
	});
	
	$('[data-toggle="tooltip"]').tooltip();
});

function buscar() {
	if(document.finder.buscador.value == "")
		swal({title: "Alerta", text: "Digite una palabra clave para la busqueda.", type: "error", confirmButtonText: "OK", html: false});
	else
		document.location = currentFile + "?buscador=" + document.finder.buscador.value;
}

function initPaginator(total) {
	$('#paginator').pagination({
		items: total,
		itemsOnPage: itemsPerPage,
		cssStyle: 'light-theme',
		onPageClick: changePage,
		currentPage: page,
		prevText: "Ant",
		nextText: "Sig"
	});
}

function changePage(currentPage) {
	var url = currentFile + "?page=" + currentPage;
	if(buscador != "")
		url += "&buscador=" + buscador;
		
	document.location = url;
}

function setDatePickerSpanish() {
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '<Ant',
		nextText: 'Sig>',
		currentText: 'Hoy',
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
		dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
		dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
		weekHeader: 'Sm',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''
	};
	$.datepicker.setDefaults($.datepicker.regional['es']);
}

function CustomDistractor(){
	this.show = function() {
		var winW = window.innerWidth;
		var winH = window.innerHeight;
		var modal = document.getElementById('modal');
		modal.style.display = "block";
		modal.style.height = winH+"px";
		
		var distractor = document.getElementById('distractor');
		distractor.style.display = "block";
		var distractorW = distractor.clientWidth;
		var distractorH = distractor.clientHeight;
		distractor.style.top = (winH/2) - (distractorH * .5)+"px";
		distractor.style.left = (winW/2) - (distractorW * .5)+"px";
	}
	this.hide = function(){
		document.getElementById('modal').style.display = "none";
		document.getElementById('distractor').style.display = "none";
	}
}