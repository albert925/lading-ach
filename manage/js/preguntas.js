
$(document).ready(function() {
	if(actionType == "none")
		initPaginator(total);
		
	$('#pageForm').ajaxForm({ beforeSubmit: validar, 
		success: function(response) { 
			Distractor.hide();
			if(checkJSON(response))
			{
				var result = JSON.parse(response);
				if(result.code != 0)
					showError(result.code, result.error);
				else
				{
					var parsedResult = result.result;
					var parentUrl = result.parentUrl;
					if(parentUrl != undefined)
						document.location = currentFile + parentUrl;
					else if(parsedResult == "")
						document.location = currentFile;
					else 
						swal({title: "Error", type: "error", confirmButtonText: "OK", html: parsedResult});
				}
			}
		}, error: function() { 
			Distractor.hide();
			swal({title: "Error", text: "Ocurrió un error inesperado, por favor inténtelo de nuevo más tarde.", type: "error", confirmButtonText: "OK", html: false});
		}
	});
});
	
function validar(formData, jqForm, options) {
	Distractor.show();
	
	var form = jqForm[0];
	error = "";
	valido = true;
	
	if(actionType != "delete")
	{
		if(form.pregunta.value == "") 
			error += "- Por favor ingrese la pregunta.<br/>";
		if(form.opcion1.value == "") 
			error += "- Por favor ingrese la opción 1.<br/>";
		if(form.opcion2.value == "") 
			error += "- Por favor ingrese la opción 2.<br/>";
		if(form.opcion3.value == "") 
			error += "- Por favor ingrese la opción 3.<br/>";
		if(form.opcion4.value == "") 
			error += "- Por favor ingrese la opción 4.<br/>";
	}
	
	if(error!="") {
		Distractor.hide();
		swal({title: "Alerta", type: "warning", confirmButtonText: "OK", html: error});
		
		valido = false;
	}
	return valido;
}