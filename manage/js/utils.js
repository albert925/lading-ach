function MM_goToURL() {
	var i, args = MM_goToURL.arguments; document.MM_returnValue = false;
	for(i=0; i<(args.length-1); i+=2) 
		eval(args[i]+".location='"+args[i+1]+"'");
}

function disableEnterKey(e) {
	var key;      
	if(window.event)
		key = window.event.keyCode; //IE
	else
		key = e.which; //firefox      
	
	return (key != 13);
}

function checkJSON(jsonString) {
	var json = null;
	try
	{
		 json = JSON.parse(jsonString);
	}
	catch(error)
	{
		 showError("10", jsonString);
	}
	 
	if(json === null)
		return false;
	else
		return true;
}

function showError(code, message) {
	Distractor.hide();
	if(code == 101)
	{
		swal({title: "Error", type: "error", confirmButtonText: "OK", html: message});
		document.location = "salir.php";
	}
	else
		swal({title: "Error", type: "error", confirmButtonText: "OK", html: message});
}

function validateFile(extensions, fileExtension) {
	var valido = false;
	for(i=0; i < extensions.length; i++) {
		if(extensions[i] == fileExtension || extensions[i].toUpperCase() == fileExtension)
			valido = true;
	}
	return valido;	
}

function validateFileSize(inputName, megabytes) {
	var valido = true;
	
	var maxBytes = 1048576 * megabytes;
	var fileSize = document.getElementById(inputName).files[0].size;
	if(fileSize > maxBytes)
		valido = false;
		
	return valido;
}

function isMobile() {
	if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4)))
    	return true;
	else
		return false;
}

function getOrientation(file, callback) {
	var reader = new FileReader();
	reader.onload = function(e) {
		var view = new DataView(e.target.result);
		if(view.getUint16(0, false) != 0xFFD8) return callback(-2);
		var length = view.byteLength, offset = 2;
		
		while (offset < length) {
			var marker = view.getUint16(offset, false);
			offset += 2;
			if (marker == 0xFFE1) {
				if (view.getUint32(offset += 2, false) != 0x45786966) return callback(-1);
				var little = view.getUint16(offset += 6, false) == 0x4949;
				offset += view.getUint32(offset + 4, little);
				var tags = view.getUint16(offset, little);
				offset += 2;
				for (var i = 0; i < tags; i++)
					if (view.getUint16(offset + (i * 12), little) == 0x0112)
						return callback(view.getUint16(offset + (i * 12) + 8, little));
			}
			else if ((marker & 0xFF00) != 0xFF00) break;
			else offset += view.getUint16(offset, false);
		}
		return callback(-1);
	};
	reader.readAsArrayBuffer(file.slice(0, 64 * 1024));
}

function loadImageURL(input) {
	var result = {};
	if(input.files && input.files[0]) {
		var reader = new FileReader();
		
		reader.onload = function (e) {
			var input = document.getElementById("imgInp");
			getOrientation(input.files[0], function(orientation) {
				result.orientation = orientation;
			});
			
			result.image = e.target.result;
		}
		
		reader.readAsDataURL(input.files[0]);
	}
	return result;
}

/******************VALIDATIONS******************/
function validteEmptyValue(formItem) {
	if(formItem.value == "")
		formItem.value = "0";
	return validateNumber(formItem.value);
}

function validateNumber(value) {
	if(value.indexOf(".") > 0)
		return false;
	else
		return !isNaN(parseInt(value)) && isFinite(value);
}

function validateEmail(email) { 
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}

function validateDate(date) {
	var pattern =/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/;
	return pattern.test(date);
}

function validateTime(time) {
	var pattern =/^([0-9]{2}):([0-9]{2}):([0-9]{2})$/;
	return pattern.test(time);
}

function fixPrice(price) {
	return price.replace(/\./g, "");
}