$(document).ready(function() {
	$('#pageForm').ajaxForm({ beforeSubmit: validar, 
		success: function(response) { 
			Distractor.hide();
			if(checkJSON(response))
			{
				var result = JSON.parse(response);
				if(result.code != 0)
					showError(result.code, result.error);
				else
				{
					var parsedResult = result.result;
					if(parsedResult != undefined)
						document.location = "videos.php";
					else
						swal({title: "Error", text: "Ocurrió un error inesperado, por favor inténtelo de nuevo más tarde.", type: "error", confirmButtonText: "OK", html: false});
				}
			}
		}, error: function() { 
			Distractor.hide();
			swal({title: "Error", text: "Ocurrió un error inesperado, por favor inténtelo de nuevo más tarde.", type: "error", confirmButtonText: "OK", html: false});
		}
	});
});
	
function validar(formData, jqForm, options) {
	Distractor.show();
	
	var form = jqForm[0];
	error = "";
	valido = true;
	
	if (form.user.value == "") 
		error += "- Por favor ingrese el usuario.<br/>";
	if (form.password.value == "")
		error += "- Por favor ingrese el password.<br/>";
	
	if (error!="") {
		Distractor.hide();
		swal({title: "Alerta", type: "warning", confirmButtonText: "OK", html: error});
		valido = false;
	}
	return valido;
}
