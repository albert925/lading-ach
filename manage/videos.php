<?php
require_once("includes/header.inc.php");
?>
<link type="text/css" rel="stylesheet" href="css/main.css"/>
<script type="text/javascript">
var itemsPerPage = <?php echo json_encode($itemsPerPage); ?>;
var actionType = <?php echo json_encode($actionType); ?>;
var page = <?php echo json_encode($page); ?>;
var idcon = <?php echo json_encode($idcon); ?>;
var currentFile = <?php echo json_encode($currentFile); ?>;
var buscador = <?php echo json_encode($buscador); ?>;
</script>
</head>
<body>
<?php if(isset($sessionToken)) { ?>
<div class="full-width full-width-extra frame">
	<div class="sidebar sidebar-fixed">
	</div>
	<div class="sidebar">
		<div id="wrapper" class="wrapper">
		</div>
	</div>
	<div class="right-content">
		<div id="navbar" class="navbar">
		</div>
		<div id="main-container" class="col-xs-12">
			<div class="col-xs-12 text-center mainTitle">
				<?php echo ucfirst(str_replace(".php", "", $currentFile)); ?>
			</div>
			<?php 
			if($actionType == "none"){ 
				$queryTotal = "SELECT COUNT(id) AS total FROM videos";
				$query = "SELECT * FROM videos ORDER BY id DESC LIMIT $inicio, $fin";
					
				if(!$resultTotal = mysqli_query($dbcon, $queryTotal)) echo mysqli_error($dbcon);
				$total = mysqli_fetch_assoc($resultTotal);
				$total = $total["total"];
			?>
			<div class="col-xs-12 col-md-10 col-md-pull-1 col-md-push-1">
				<div class="col-xs-6 separator-top">
					<a href="<?php echo $currentFile; ?>?actionType=addVideo&page=<?php echo $page; ?>" target="_self" class="addLink">
						<i class="fa fa-plus plusLbl"></i>&nbsp;&nbsp;Ingresar nuevo video
					</a>
				</div>
				<div class="col-xs-6 separator-top">
					<a href="<?php echo $currentFile; ?>?actionType=addMultiple&page=<?php echo $page; ?>" target="_self" class="addLink">
						<i class="fa fa-plus plusLbl"></i>&nbsp;&nbsp;Ingresar varios videos
					</a>
				</div>
				<div class="col-xs-12 separator-top">
				</div>
				<?php
				if(!$result = mysqli_query($dbcon, $query)) echo mysqli_error($dbcon);
				while($row = mysqli_fetch_array($result)){
				?>
				<div class="col-xs-6 col-md-4 col-lg-3 containerGaleria" data-imgid="<?php echo $row['id']; ?>">
					<img src="<?php echo $row['imagen']; ?>" class="img-responsive img-rounded" style="float:left" alt="" border="0"/>
					<div id="opcionesGaleria<?php echo $row['id']; ?>" class="opcionesGaleria">
						<a href="<?php echo $currentFile; ?>?actionType=editVideo&page=<?php echo $page; ?>&idcon=<?php echo $row['id']; ?>"
						target="_self" data-toggle="tooltip" title="Editar">
						<button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a>
						<a href="<?php echo $currentFile; ?>?actionType=deleteVideo&page=<?php echo $page; ?>&idcon=<?php echo $row['id']; ?>"
						target="_self" data-toggle="tooltip" title="Eliminar">
						<button class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></button></a>
					</div>
				</div>
				<?php } ?>
				<div id="paginator" class="col-xs-12">
				</div>
			</div>
			<?php echo "<script> var total = ".$total."; </script>"; }else if($actionType == "addVideo"){ ?>
			<div class="col-xs-12 col-md-10 col-md-pull-1 col-md-push-1">
				<form id="pageForm" name="pageForm" role="form" class="form-horizontal" enctype="multipart/form-data" action="services/VideosService.php" method="post">
					<div class="form-group required">
						<label for="titulo" class="control-label col-xs-3">Título</label>
						<div class="col-xs-9">
							<input id="titulo" name="titulo" type="text" class="form-control">
						</div>
					</div>
					<div class="form-group required">
						<label for="video" class="control-label col-xs-3">Link video (Youtube)</label>
						<div class="col-xs-9">
							<input id="video" name="video" type="text" class="form-control">
						</div>
					</div>
					<input id="page" name="page" type="hidden" value="<?php echo $page; ?>"/>
					<input id="actionType" name="actionType" type="hidden" value="addVideo"/>
					<div class="text-center">
						<input type="button" onclick="MM_goToURL('self','<?php echo $currentFile."?page=".$page; ?>');return document.MM_returnValue" 
							value="Volver" class="btn btn-lg btn-success"/>
						<input type="submit" value="Guardar" class="btn btn-lg btn-success"/>
					</div>
				</form>
			</div>
			<?php 
			}else if($actionType == "editVideo"){ 
				$query = "SELECT * FROM videos WHERE id='$idcon'";
				if(!$res = mysqli_query($dbcon, $query)) echo mysqli_error($dbcon);
				$result = mysqli_fetch_array($res);
			?>
			<div class="col-xs-12 col-md-10 col-md-pull-1 col-md-push-1">
				<form id="pageForm" name="pageForm" role="form" class="form-horizontal" enctype="multipart/form-data" action="services/VideosService.php" method="post">
					<div class="form-group required">
						<label for="titulo" class="control-label col-xs-3">Título</label>
						<div class="col-xs-9">
							<input id="titulo" name="titulo" type="text" class="form-control" value="<?php echo $result['titulo']; ?>">
						</div>
					</div>
					<?php if($result['video'] != "") { ?>
					<div class="form-group">
						<label for="video" class="control-label col-xs-3">Video actual</label>
						<div class="col-xs-9 col-md-8 col-lg-6">
							<iframe width="100%" height="300" src="http://www.youtube.com/embed/<?php echo $result['video']; ?>" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
					<?php } ?>
					<div class="form-group">
						<label for="video" class="control-label col-xs-3">Link video (Youtube)</label>
						<div class="col-xs-9">
							<input id="video" name="video" type="text" class="form-control">
						</div>
					</div>
					<input id="page" name="page" type="hidden" value="<?php echo $page; ?>"/>
					<input id="idcon" name="idcon" type="hidden" value="<?php echo $idcon; ?>"/>
					<input name="actionType" type="hidden" id="actionType" value="editVideo"/>
					<div class="text-center">
						<input type="button" onclick="MM_goToURL('self','<?php echo $currentFile."?page=".$page; ?>');return document.MM_returnValue" 
							value="Volver" class="btn btn-lg btn-success"/>
						<input type="submit" value="Guardar" class="btn btn-lg btn-success"/>
					</div>
				</form>
			</div>
			<?php }else if($actionType == "deleteVideo"){ ?>
			<div class="col-xs-12 col-md-10 col-md-pull-1 col-md-push-1">
				<form id="pageForm" name="pageForm" role="form" class="form-horizontal" enctype="multipart/form-data" action="services/VideosService.php" method="post">
					<div class="col-xs-12 text-center separator-bottom">
						<strong><font color="#000000">&iquest;Esta seguro de eliminar este contenido del sistema?</font></strong>
					</div>
					<input id="page" name="page" type="hidden" value="<?php echo $page; ?>"/>
					<input id="idcon" name="idcon" type="hidden" value="<?php echo $idcon; ?>"/>
					<input name="actionType" type="hidden" id="actionType" value="deleteVideo"/>
					<div class="text-center">
						<input type="button" onclick="MM_goToURL('self','<?php echo $currentFile."?page=".$page; ?>');return document.MM_returnValue" 
							value="No" class="btn btn-lg btn-success"/>
						<input type="submit" value="Si" class="btn btn-lg btn-success"/>
					</div>
				</form>
			</div>
			<?php }else if($actionType == "addMultiple"){ ?>
			<div class="col-xs-12 col-md-10 col-md-pull-1 col-md-push-1">
				<div class="col-xs-12 separator-top separator-bottom">
					<a onClick="addVideo();" target="_self" href="#" class="addLink"><i class="fa fa-plus plusLbl"></i>&nbsp;&nbsp;Nuevo video</a>
				</div>
				<form id="pageForm" name="pageForm" role="form" class="form-horizontal" enctype="multipart/form-data" action="services/VideosService.php" method="post">
					<div id="formContainer">
						<div class="form-group required">
							<label for="titulo1" class="control-label col-xs-3">Título</label>
							<div class="col-xs-9">
								<input id="titulo1" name="titulo1" type="text" class="form-control">
							</div>
						</div>
						<div class="form-group required">
							<label for="video1" class="control-label col-xs-3">Link Video (Youtube)</label>
							<div class="col-xs-9">
								<input id="video1" name="video1" type="text" class="form-control">
							</div>
						</div>
						<hr>
						<div class="form-group required">
							<label for="titulo2" class="control-label col-xs-3">Título</label>
							<div class="col-xs-9">
								<input id="titulo2" name="titulo2" type="text" class="form-control">
							</div>
						</div>
						<div class="form-group required">
							<label for="video2" class="control-label col-xs-3">Link Video (Youtube)</label>
							<div class="col-xs-9">
								<input id="video2" name="video2" type="text" class="form-control">
							</div>
						</div>
					</div>
					<input id="numVid" name="numVid" type="hidden" value="2"/>
					<input id="page" name="page" type="hidden" value="<?php echo $page; ?>"/>
					<input id="actionType" name="actionType" type="hidden" value="addVideos"/>
					<div class="text-center">
						<input type="button" onclick="MM_goToURL('self','<?php echo $currentFile."?page=".$page; ?>');return document.MM_returnValue" 
							value="Volver" class="btn btn-lg btn-success"/>
						<input type="submit" value="Guardar" class="btn btn-lg btn-success"/>
					</div>
				</form>
			</div>
			<?php } ?>
		</div>
		<!--main-container-->
	</div>
	<!--right-content-->
</div>
<?php }else{ ?>
<div id="lostConnection">
<?php require_once("utils/components/lostConnection.php"); ?>
</div>
<?php } require_once("includes/footer.inc.php"); ?>
<script type="text/javascript" src="js/videos.js"></script>
</body>
</html>