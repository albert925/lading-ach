<?php
	require_once("connect.php");
	require_once("plugins/Zebra_Image.php");
	require_once("utils/constants.php");
	require_once("utils/utils.php");
	
	$headers = '<meta name="title" content="Landing SOI Screensaver Admin">
				<meta name="description" content="Landing SOI Screensaver Admin">
				<meta name="author" content="Gran Tec">';
	
	$titleAdmin = "Landing SOI Screensaver Admin";
	
	//Session Vars
	$prefix = "landingSOI";
	$sessionUserID = $prefix."AdminUserID";
	$sessionTokenID = $prefix."AdminSession";
	$profilelUserID = $prefix."AdminProfile";
	
	$sessionToken = @$_SESSION[$sessionTokenID];
	$sessionUser = @$_SESSION[$sessionUserID];
	$sessionProfile = @$_SESSION[$profilelUserID];
	
	//Config Vars
	$devMode = TRUE;
	$debugServices = TRUE;
	$itemsPerPage = 25;
	
	$local = TRUE;
	
	if($local)
		$rootUrl = "http://localhost/ach/landing_soi_screensaver/";
	else
		$rootUrl = "http://server_root";
?>