<?php
require_once("includes/header.inc.php");
?>
<link type="text/css" rel="stylesheet" href="css/main.css"/>
<script type="text/javascript">
var currentFile = <?php echo json_encode($currentFile); ?>;
</script>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-xs-10 col-xs-push-1 col-xs-pull-1 col-md-4 col-md-push-4 col-md-pull-4 text-center">
			<img src="images/logo.png" alt="" border="0" class="logoIndex img-responsive"/>
			<form id="pageForm" name="pageForm" role="form" class="form-horizontal" enctype="multipart/form-data" action="services/UpdateMemberPasswordService.php" method="post">
				<div class="form-group">
					<label for="user" class="control-label sr-only">Usuario</label>
					<div class="col-xs-12">
						<select id="user" name="user" class="form-control">
							<?php
							$query = "SELECT user FROM member";
							if(!$result = mysqli_query($dbcon, $query)) echo mysqli_error($dbcon);
							
							while($row = mysqli_fetch_assoc($result))
								echo '<option value="'.$row['user'].'">'.$row['user'].'</option>';
						?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="password" class="control-label sr-only">Password</label>
					<div class="col-xs-12">
						<input id="password" name="password" type="password" class="form-control" placeholder="Password">
					</div>
				</div>
				<input type="submit" value="Cambiar Password" class="btn btn-lg btn-success">
			</form>
		</div>
	</div>
</div>
<?php require_once("includes/footer.inc.php"); ?>
<script type="text/javascript">
$(document).ready(function() {
	$('#pageForm').ajaxForm({ beforeSubmit: validar,
		success: function(response) {
			if(checkJSON(response))
			{
				var result = JSON.parse(response);
				if(result.code != 0)
					showError(result.code, result.error);
				else {
					var parsedResult = result.result;
					document.pageForm.password.value = "";
					Distractor.hide();
					swal({title: "Éxito", type: "success", confirmButtonText: "OK", html: parsedResult});
				}
			}
		}, error: function() { 
			Distractor.hide();
			swal({title: "Error", text: "Ocurrió un error inesperado, por favor inténtelo de nuevo más tarde.", type: "error", confirmButtonText: "OK", html: false});
		}
	});
});

function validar(formData, jqForm, options) {
	Distractor.show();
	
	var form = jqForm[0];
	error = "";
	valido = true;
	
	if(form.user.value == "") 
		error += "- Por favor ingrese un nombre de usuario.<br/>";
	if(form.password.value == "") 
		error += "- Por favor ingrese el nuevo password.<br/>";
	
	if(error != "") {
		Distractor.hide();
		swal({title: "Alerta", type: "warning", confirmButtonText: "OK", html: error});
		valido = false;
	}
	return valido;
}
</script>
</body>
</html>
