<?php
class YouTube {
	function GetVideoIdFromUrl($url) {
		if (preg_match('/youtube\.com\/watch\?v=([^\&\?\/]+)/', $url, $id)) {
			return $values = $id[1];
		} else if (preg_match('/youtube\.com\/embed\/([^\&\?\/]+)/', $url, $id)) {
			return $values = $id[1];
		} else if (preg_match('/youtube\.com\/v\/([^\&\?\/]+)/', $url, $id)) {
			return $values = $id[1];
		} else if (preg_match('/youtu\.be\/([^\&\?\/]+)/', $url, $id)) {
			return $values = $id[1];
		}
		else if (preg_match('/youtube\.com\/verify_age\?next_url=\/watch%3Fv%3D([^\&\?\/]+)/', $url, $id)) {
			return $values = $id[1];
		} else {   
			return false;
		}
	}

	function EmbedVideo($videoid,$width = 425,$height = 350) {
		$videoid = $this->GetVideoIdFromUrl($videoid);
		return '<object width="'.$width.'" height="'.$height.'"><param name="movie" value="http://www.youtube.com/v/'.$videoid.'"></param><param name="wmode" value="transparent"></param><embed src="http://www.youtube.com/v/'.$videoid.'" type="application/x-shockwave-flash" wmode="transparent" width="'.$width.'" height="'.$height.'"></embed></object>';
	}

	function GetImg($videoid,$imgid = 1) {
		//$videoid = $this->GetVideoIdFromUrl($videoid);
		//$videoid = str_replace('?fs=1','',$videoid);
		return "http://i4.ytimg.com/vi/$videoid/$imgid.jpg";
	}
	
	function ShowImg($videoid,$imgid = 2) {
		return $this->GetImg($videoid,$imgid);
	}
}
?>
