<?php
require_once("includes/header.inc.php");
?>
<link type="text/css" rel="stylesheet" href="css/main.css"/>
<script type="text/javascript">
var itemsPerPage = <?php echo json_encode($itemsPerPage); ?>;
var actionType = <?php echo json_encode($actionType); ?>;
var page = <?php echo json_encode($page); ?>;
var idcon = <?php echo json_encode($idcon); ?>;
var currentFile = <?php echo json_encode($currentFile); ?>;
var buscador = <?php echo json_encode($buscador); ?>;
</script>
</head>
<body>
<?php if(isset($sessionToken)) { ?>
<div class="full-width full-width-extra frame">
	<div class="sidebar sidebar-fixed"></div>
	<div class="sidebar">
		<div id="wrapper" class="wrapper">
		</div>
	</div>
	<div class="right-content">
		<div id="navbar" class="navbar">
		</div>
		<div id="main-container" class="col-xs-12">
			<div class="col-xs-12 text-center mainTitle">
				<?php echo ucfirst(str_replace(".php", "", $currentFile)); ?>
			</div>
			<?php
			if($actionType == "none"){
				if($buscador != ""){
			 		$queryTotal = "SELECT COUNT(id) AS total FROM preguntas WHERE UPPER(pregunta)";
					$query = "SELECT * FROM preguntas WHERE UPPER(pregunta) ORDER BY id DESC LIMIT $inicio, $fin";
				}
				else{
					$queryTotal = "SELECT COUNT(id) AS total FROM preguntas";
					$query = "SELECT * FROM preguntas ORDER BY id DESC LIMIT $inicio, $fin";
				}
				
				if(!$resultTotal = mysqli_query($dbcon, $queryTotal)) echo mysqli_error($dbcon);
				$total = mysqli_fetch_assoc($resultTotal);
				$total = $total["total"];
			?>
			<div class="col-xs-12">
				<form id="finder" name="finder" method="post" action="" class="form-inline" role="form">
					<div id="searchGroup" class="form-group separator-right separator-bottom col-xs-12">
						<div class="input-group">
							<span class="input-group-btn">
								<button class="btn btn-default" type="button" style="margin-top:0"
									onclick="return buscar();" onKeyPress="return buscar();" ><i class="fa fa-search"></i></button>
							</span>
							<input id="buscador" name="buscador" type="text" class="form-control" placeholder="Buscar...">
							<span class="input-group-btn">
								<button type="button" class="btn btn-default" style="margin-top:0"
									onclick="MM_goToURL('self','<?php echo $currentFile; ?>');return document.MM_returnValue">Ver todos</button>
							</span>
						</div>
					</div>
				</form>
				<div class="margin-gral">
					<a href="<?php echo $currentFile; ?>?actionType=add&page=<?php echo $page; ?>" target="_self" class="addLink">
						<i class="fa fa-plus plusLbl"></i>&nbsp;&nbsp;Ingresar nueva pregunta
					</a>
				</div>
				<div class="table-responsive separator-top">
					<table id="mainTable" class="table table-hover">
						<thead>
							<tr>
								<th>Pregunta</th>
								<th>Opción 1</th>
                                <th>Opción 2</th>
                                <th>Opción 3</th>
                                <th>Opción 4</th>
                                <th>Correcta</th>
								<th class="col-xs-1">Opciones</th>
							</tr>
						</thead>
						<tbody>
							<?php
							if(!$result = mysqli_query($dbcon, $query)) echo mysqli_error($dbcon);
							while($row = mysqli_fetch_array($result)){
							?>
							<tr>
								<td><?php echo $row['pregunta']; ?></td>
								<td><?php echo $row['opcion1']; ?></td>
                                <td><?php echo $row['opcion2']; ?></td>
                                <td><?php echo $row['opcion3']; ?></td>
                                <td><?php echo $row['opcion4']; ?></td>
                                <td><?php echo $row['correcta']; ?></td>
								<td>
									<a href="<?php echo $currentFile; ?>?actionType=edit&page=<?php echo $page; ?>&idcon=<?php echo $row['id']; ?>"
									target="_self" data-toggle="tooltip" title="Editar">
									<button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a>
									<a href="<?php echo $currentFile; ?>?actionType=delete&page=<?php echo $page; ?>&idcon=<?php echo $row['id']; ?>"
									target="_self" data-toggle="tooltip" title="Eliminar">
									<button class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></button></a>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
				<div id="paginator" class="col-xs-12">
				</div>
			</div>
			<?php echo "<script> var total = ".$total."; </script>"; } else if($actionType == "add"){ ?>
			<div class="col-xs-12 col-md-10 col-md-pull-1 col-md-push-1">
				<form id="pageForm" name="pageForm" role="form" class="form-horizontal" enctype="multipart/form-data" action="services/PreguntasService.php" method="post">
					<div class="form-group required">
						<label for="pregunta" class="control-label col-xs-3">Pregunta</label>
						<div class="col-xs-9">
							<input id="pregunta" name="pregunta" type="text" class="form-control">
						</div>
					</div>
					<div class="form-group required">
						<label for="opcion1" class="control-label col-xs-3">Opción 1</label>
						<div class="col-xs-9">
							<input id="opcion1" name="opcion1" type="text" class="form-control">
						</div>
					</div>
                    <div class="form-group required">
						<label for="opcion2" class="control-label col-xs-3">Opción 2</label>
						<div class="col-xs-9">
							<input id="opcion2" name="opcion2" type="text" class="form-control">
						</div>
					</div>
                    <div class="form-group required">
						<label for="opcion3" class="control-label col-xs-3">Opción 3</label>
						<div class="col-xs-9">
							<input id="opcion3" name="opcion3" type="text" class="form-control">
						</div>
					</div>
                    <div class="form-group required">
						<label for="opcion4" class="control-label col-xs-3">Opción 4</label>
						<div class="col-xs-9">
							<input id="opcion4" name="opcion4" type="text" class="form-control">
						</div>
					</div>
                    <div class="form-group">
						<label for="correcta" class="control-label col-xs-3">Correcta</label>
						<div class="col-xs-9">
							<div class="radio-inline">
								<label>
									<input name="correcta" type="radio" value="1" checked> Opción 1
								</label>
							</div>
							<div class="radio-inline">
								<label>
									<input name="correcta" type="radio" value="2"> Opción 2
								</label>
							</div>
							<div class="radio-inline">
								<label>
									<input name="correcta" type="radio" value="3"> Opción 3
								</label>
							</div>
                            <div class="radio-inline">
								<label>
									<input name="correcta" type="radio" value="4"> Opción 4
								</label>
							</div>
						</div>
					</div>
					<input id="page" name="page" type="hidden" value="<?php echo $page; ?>"/>
					<input id="actionType" name="actionType" type="hidden" value="add"/>
					<div class="text-center">
						<input type="button" onclick="MM_goToURL('self','<?php echo $currentFile."?page=".$page; ?>');return document.MM_returnValue" 
							value="Volver" class="btn btn-lg btn-success"/>
						<input type="submit" value="Guardar" class="btn btn-lg btn-success"/>
					</div>
				</form>
			</div>
			<?php 
			}else if($actionType == "edit"){ 
				$query = "SELECT * FROM preguntas WHERE id='$idcon'";
				if(!$res = mysqli_query($dbcon, $query)) echo mysqli_error($dbcon);
				$result = mysqli_fetch_array($res);
			?>
			<div class="col-xs-12 col-md-10 col-md-pull-1 col-md-push-1">
				<form id="pageForm" name="pageForm" role="form" class="form-horizontal" enctype="multipart/form-data" action="services/PreguntasService.php" method="post">
					<div class="form-group required">
						<label for="pregunta" class="control-label col-xs-3">Pregunta</label>
						<div class="col-xs-9">
							<input id="pregunta" name="pregunta" type="text" class="form-control" value="<?php echo $result['pregunta']; ?>">
						</div>
					</div>
					<div class="form-group required">
						<label for="opcion1" class="control-label col-xs-3">Opción 1</label>
						<div class="col-xs-9">
							<input id="opcion1" name="opcion1" type="text" class="form-control" value="<?php echo $result['opcion1']; ?>">
						</div>
					</div>
                    <div class="form-group required">
						<label for="opcion2" class="control-label col-xs-3">Opción 2</label>
						<div class="col-xs-9">
							<input id="opcion2" name="opcion2" type="text" class="form-control" value="<?php echo $result['opcion2']; ?>">
						</div>
					</div>
                    <div class="form-group required">
						<label for="opcion3" class="control-label col-xs-3">Opción 3</label>
						<div class="col-xs-9">
							<input id="opcion3" name="opcion3" type="text" class="form-control" value="<?php echo $result['opcion3']; ?>">
						</div>
					</div>
                    <div class="form-group required">
						<label for="opcion4" class="control-label col-xs-3">Opción 4</label>
						<div class="col-xs-9">
							<input id="opcion4" name="opcion4" type="text" class="form-control" value="<?php echo $result['opcion4']; ?>">
						</div>
					</div>
                    <div class="form-group">
						<label for="correcta" class="control-label col-xs-3">Correcta</label>
						<div class="col-xs-9">
							<div class="radio-inline">
								<label>
									<input name="correcta" type="radio" value="1" <?php if($result['correcta'] == "1") echo "checked"; ?>> Opción 1
								</label>
							</div>
							<div class="radio-inline">
								<label>
									<input name="correcta" type="radio" value="2" <?php if($result['correcta'] == "2") echo "checked"; ?>> Opción 2
								</label>
							</div>
							<div class="radio-inline">
								<label>
									<input name="correcta" type="radio" value="3" <?php if($result['correcta'] == "3") echo "checked"; ?>> Opción 3
								</label>
							</div>
                            <div class="radio-inline">
								<label>
									<input name="correcta" type="radio" value="4" <?php if($result['correcta'] == "4") echo "checked"; ?>> Opción 4
								</label>
							</div>
						</div>
					</div>
					<input id="page" name="page" type="hidden" value="<?php echo $page; ?>"/>
					<input id="idcon" name="idcon" type="hidden" value="<?php echo $idcon; ?>"/>
					<input id="actionType" name="actionType" type="hidden" value="edit"/>
					<div class="text-center">
						<input type="button" onclick="MM_goToURL('self','<?php echo $currentFile."?page=".$page; ?>');return document.MM_returnValue" 
							value="Volver" class="btn btn-lg btn-success"/>
						<input type="submit" value="Guardar" class="btn btn-lg btn-success"/>
					</div>
				</form>
			</div>
			<?php }else if($actionType == "delete"){ ?>
			<div class="col-xs-12 col-md-10 col-md-pull-1 col-md-push-1">
				<form id="pageForm" name="pageForm" role="form" class="form-horizontal" enctype="multipart/form-data" action="services/PreguntasService.php" method="post">
					<div class="col-xs-12 text-center separator-bottom">
						<strong><font color="#000000">&iquest;Esta seguro de eliminar este contenido del sistema?</font></strong>
					</div>
					<input id="page" name="page" type="hidden" value="<?php echo $page; ?>"/>
					<input id="idcon" name="idcon" type="hidden" value="<?php echo $idcon; ?>"/>
					<input id="actionType" name="actionType" type="hidden" value="delete"/>
					<div class="text-center">
						<input type="button" onclick="MM_goToURL('self','<?php echo $currentFile."?page=".$page; ?>');return document.MM_returnValue" 
							value="No" class="btn btn-lg btn-success"/>
						<input type="submit" value="Si" class="btn btn-lg btn-success"/>
					</div>
				</form>
			</div>
			<?php } ?>
		</div>
		<!--main-container-->
	</div>
	<!--right-content-->
</div>
<?php }else{ ?>
<div id="lostConnection">
<?php require_once("utils/components/lostConnection.php"); ?>
</div>
<?php } require_once("includes/footer.inc.php"); ?>
<script type="text/javascript" src="js/preguntas.js"></script>
</body>
</html>