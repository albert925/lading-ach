<div id="distractorContainer">
<?php require_once("utils/components/distractor.php"); ?>
</div>
<?php
echo loadJSLibrary("js/", "lib/jquery-1.11.1", $devMode);
echo loadJSLibrary("js/", "lib/jquery.form", $devMode);
echo loadJSLibrary("js/", "lib/jquery-ui", $devMode);
echo loadJSLibrary("js/", "lib/bootstrap", $devMode);
echo loadJSLibrary("js/", "lib/sweetalert2", $devMode);
echo loadJSLibrary("js/", "lib/jquery.simplePagination", $devMode);
echo loadJSLibrary("js/", "main", $devMode);
echo loadJSLibrary("js/", "utils", $devMode);
?>