<?php
session_start();
require_once("config.php");

$currentFile = basename($_SERVER['PHP_SELF']);

$actionType = getGetParam($dbcon, 'actionType');
if($actionType == ""){
	$actionType = getPostParam($dbcon, 'actionType');
	if($actionType == ""){
		$actionType = "none";
	}
}

$page = getGetParam($dbcon, 'page');
if($page != "")
	$inicio = ($page - 1) * $itemsPerPage;
else
	$inicio = 0;

$fin = $itemsPerPage;

$idcon = getGetParam($dbcon, 'idcon');
$buscador = getGetParam($dbcon, 'buscador');
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<?php echo $headers; ?>
<title><?php echo $titleAdmin; ?></title>
<link type="text/css" rel="stylesheet" href="css/bootstrap.min.css">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->